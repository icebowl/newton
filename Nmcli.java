//Nmci code by CWColeman
//java Nmcli 200 200 200 -1 -2 -3 100 100 6
// java Nmcli startRed startGreen startBlue  redIncrement greenIncrement blueIncrement color scale power

import java.awt.*;
import java.awt.event.*; 
import javax.swing.*;

public class Nmcli extends JFrame { 
	
  
	class DivideImaginary {
		double den,rnum, inum;
		DivideImaginary(double na,double nbi,double da, double dbi){
				den = ((da*da)+(dbi*dbi));	
				rnum = ((na*da) + (nbi*dbi))/den;                                                
				inum = ((na*(-1*dbi))+(nbi*da))/den;
			
					}		
				double a(){
					return  rnum;
				}
					double b(){
					return  inum;
				}
	}//end DivideImaginary
	
	
	class INewtonMethod {
		int r; //double n; 
		double val = 0;
		int count = 0;
		// radial y radican x
		INewtonMethod (double a, double bi, int root, long limit){
			double x0 = a;
			double yi0 = bi;
			int n = root;
			double x1,yi1,na,nbi, da, dbi;
			int i;
			long x0long, y0ilong,x1long, y1ilong, longlimit = limit;
			boolean done = false;
			
			while (!done){
			
				Iproduct ipn = new Iproduct(x0,yi0,n);
				na = ipn.avalue() - 1; nbi = ipn.bvalue(); 
				Iproduct ipd = new Iproduct(x0,yi0,n-1);
				da = n* ipd.avalue(); dbi = n* ipd.bvalue(); 
				DivideImaginary di = new DivideImaginary(na,nbi,da,dbi);
				x1 = x0 - di.a(); 
				yi1 = yi0 - di.b();
				x0long = (long) (x0 * longlimit);		
				x1long= (long) (x1 * longlimit);
				y0ilong = (long) (yi0 * longlimit);		
				y1ilong= (long)( yi1 * longlimit);
				count++;
							 // 
				if (((x0long == x1long) & (y0ilong == y1ilong))|| (count > 255))done = true;
				x0 = x1; yi0 = yi1;
										 
			}
		}
		int value (){  
			return count;
		}
	}//INewtonMethod
	
	class Iproduct{
		double return_a, return_b;
		Iproduct (double a, double bi,int exp){
			double a2,bi2,atmp,btmp;
			int i;
			if (exp == 1){
				return_a = a; return_b = bi;
			}else{
				a2 = ((a*a) - (bi*bi));
				bi2 = (2*a*bi);
				atmp = a2; btmp = bi2;
				for (i = 1; i < (exp-1); i++){
					atmp =  ((a2*a) - (bi2*bi));
					bi2 = ((a*bi2) + (a2*bi));
					a2 = atmp;		
				
				}
			
				return_a = a2; return_b = bi2;
				}
			}
			double avalue(){
			  return return_a;
			}
			double bvalue(){
			return return_b;
			}
	}//end Iproduct
	
	

   public Nmcli()
   {
      super( "NewtonMethod" );
      setSize(1200,800);  
      setVisible( true );   
   }
// globals

	class ColorArray{
		int rgb[][] = new int[3][256]; 
		int[][] color(){
		int i;
		int colors = ilimit;
		// -- build array
		double startR,endR, startG,endG, startB, endB;
		double redD, greenD, blueD;
		startR = (double) ired;	endR = (double) iradj;  
		startG = (double)igreen; endG = (double)igadj ;
		startB = (double)iblue ; endB = (double)ibadj;
		redD = (endR - startR)/ colors;
		greenD = (endG - startG)/colors;
		blueD = (endB - startB)/colors;
		 System.out.println(ired+" \t"+igreen+"\t"+iblue);
		  System.out.println(iradj+" \t"+igadj+"\t"+ibadj);
		 System.out.println(redD+" \t"+greenD+"\t"+blueD);
		 
		for (i = 0; i < 256;i++){
			rgb[0][i] = (int)startR;
			rgb[1][i] = (int)startG;
			rgb[2][i] = (int)startB;
			
		
			startR = startR + redD;
			startG = startG +greenD;
			startB = startB +blueD;
			// System.out.println(startR+" \t"+startG+"\t"+startB);
			
		if (startR > 255) startR = (double) ired;if (startR < 0) startR = (double) ired;
		if (startG > 255) startG = (double) igreen;if (startG < 0) startG = (double) igreen;
		if (startB > 255) startB = (double) iblue;if (startB < 0) startB =  (double) iblue;
	}
		
		
		/////////////////////////
		
				return rgb;
		}
	
	}//end ColorArray

   public void paint( Graphics g )
   {
	   	int rgb[][] = new int[3][256]; 
   // this algorithm written by Craig Coleman
		 int h,k,c,red,green,blue;
		 double x,y;
		 red = 0; green = 0; blue = 0;
		 int mc;
		//g.setColor(new Color(red,green,blue));
		//		g.drawRect(10,10,570,570);
		ColorArray ca = new ColorArray();
		rgb = ca.color();
		int scale = iscale;
		c = 0;
		int max = 0;
		for (h = -400; h <= 400; h ++ ){
		for (k = -400; k <= 400; k++){
		x =(double)h/scale;
		y =(double)k/scale;

		INewtonMethod n = new INewtonMethod(x,y,ipower,1000000L);
		c = n.value();
		//System.out.println(max);
		//c++;
				mc = c % 256;
				if (mc > max) max = mc;
		//System.out.println(mc);
					red = rgb[0][mc];
					green= rgb[1][mc];
					blue = rgb[2][mc];
					g.setColor(new Color(red,green,blue));
					g.drawLine(h+400,400-k,h+400,400-k);
							 
					
	}//end for h
	}//end for k
		
	for (int b = 0; b < 256; b++){
					red = rgb[0][b];
					green= rgb[1][b];
					blue = rgb[2][b];
					g.setColor(new Color(red,green,blue));
					g.drawLine(810,b+100,860,b+100);
	}
	g.setColor(new Color(0,0,0));
	g.drawString("Newton's method of approximating the imaginary roots to x^4 -1", 50,820);
	stopFlag = true;
	
	}//end paint

  // execute application
   static int ired , igreen , iblue; 
   static int iradj,igadj,ibadj,ilimit,iscale, ipower;
	boolean stopFlag;
   public static void main( String a[] )
   {
	   ired =  Integer.parseInt(a[0]);
	   igreen =  Integer.parseInt(a[1]);
	   iblue =  Integer.parseInt(a[2]);
	   iradj =  Integer.parseInt(a[3]);
	   igadj=  Integer.parseInt(a[4]);
	   ibadj =  Integer.parseInt(a[5]);
	   ilimit =  Integer.parseInt(a[6]);//this will be the number of colors
	   iscale =  Integer.parseInt(a[7]);
	   ipower =  Integer.parseInt(a[8]);
	   
	   Nmcli myobject = new Nmcli();//change this 
	    System.out.println(ired+" "+igreen+" "+iblue);
	    System.out.println(iradj+" "+igadj+" "+ibadj);
	    System.out.println(ilimit+ " "+iscale);
	   	// adapter to handle only windowClosing event
		myobject.addWindowListener(
			new WindowAdapter() {
				public void windowClosing( WindowEvent event )
				{System.exit( 0 );}
			}  // end anonymous inner class
		); // end call to addWindowListener
   }
}// end class Painter

